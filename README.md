# Time Series Studies

A project with a set of studies about time series analysis

## References

* [Python for Time Series Data Analysis](https://www.udemy.com/course/python-for-time-series-data-analysis/)

    <em>Copyright Pierian Data</em>

    <em>For more information, visit us at <a href='http://www.pieriandata.com'>www.pieriandata.com</a></em>

* [Series Temporais com Python](https://www.udemy.com/course/series-temporais-com-python/)